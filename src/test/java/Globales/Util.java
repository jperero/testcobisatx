package Globales;

import io.appium.java_client.windows.WindowsDriver;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;


public class Util {

    public static WindowsDriver driver;
    public static WebDriverWait wait;
    static JavascriptExecutor js;
    public static Properties prop = null;

    private static String[] dataCliente;
    public static String[] getDataCliente() {
        return dataCliente;
    }
    public static void setDataCliente(String[] datos) {
        Util.dataCliente = datos;
    }

    public static File captura = null;

    static void InicioConfig() {
        prop = new Properties();
        try {
            prop.load(new FileInputStream(System.getProperty("user.dir")+"/config.properties"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void Inicio(String nom_archivo) throws MalformedURLException {
        //Set the Desired Capabilities
        InicioConfig();
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName", "Windows");
        caps.setCapability("deviceName", "WindowsPC");
        caps.setCapability("app", prop.getProperty("ruta_exe"));

        driver = new WindowsDriver(new URL("http://0.0.0.0:4723/wd/hub"), caps);
        driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);

        Reporte.setNombreArchivo(nom_archivo);
        Reporte.setNombreReporte("");
        Reporte.setEntorno("");
    }

    public static List<String> getCamposDataPool(String fileName)
    {
        List<String> lines = Collections.emptyList();
        try
        {
            lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return lines;
    }

    public static String[] getCamposDataPool(String fileName, String numregistro )
    {
        List<String> lines = Collections.emptyList();
        String[] campos = null;
        try
        {
            lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
            Iterator<String> itr = lines.iterator();

            while (itr.hasNext())
            {
                campos = itr.next().split("\t");
                if (campos[0].equals(numregistro))
                {
                    break;
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return campos;
    }

    public static void CapturarImagen()
    {
        TimeZone tz = TimeZone.getTimeZone("EST"); // or PST, MID, etc ...
        Date now = new Date();
        DateFormat df = new SimpleDateFormat("yyyyMMddhhmmss");
        df.setTimeZone(tz);
        String currentTime = df.format(now);
        Util.captura = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    }

    public static void assert_igual(String caso, String detalle, String actual, String esperado, Boolean captura, String categoria)
    {
        try
        {
            assertThat(actual, is(esperado));
            Reporte.agregarPaso(caso, detalle, actual, esperado, captura, categoria);
        }
        catch (AssertionError e) {
            Reporte.agregarPaso(caso, detalle, e.getMessage(), esperado, captura, 1, categoria);
            System.out.println(e.getMessage());
            Assert.fail();
        }
    }

    public static void assert_contiene(String caso, String detalle, String actual, String esperado, Boolean captura, String categoria)
    {
        try
        {
            assertThat(actual, CoreMatchers.containsString(esperado));
            Reporte.agregarPaso(caso, detalle, actual, esperado, captura, categoria);
        }
        catch (AssertionError e) {
            Reporte.agregarPaso(caso, detalle, e.getMessage(), esperado, captura, 1, categoria);
            System.out.println(e.getMessage());
            Assert.fail();
        }
    }

    public static void assert_contiene(String caso, String detalle, String actual, String esperado1, String esperado2, Boolean captura, String categoria)
    {
        try
        {
            assertThat(actual, either(containsString(esperado1)).or(containsString(esperado2)));
            Reporte.agregarPaso(caso, detalle, actual, "", captura, categoria);
        }
        catch (AssertionError e) {
            Reporte.agregarPaso(caso, detalle, e.getMessage(), "", captura, 1, categoria);
            System.out.println(e.getMessage());
            Assert.fail();
        }
    }

    public static void waitForElementToBeClickable(By locator) {
        wait = new WebDriverWait(Util.driver,30,100);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }



}
