package TestPages;

import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import java.util.List;

public class Login {
    String[] datos = null;
    public Boolean aperturarCaja = false;

    void cargaDatosLogin(String seclogin) {
        if (seclogin.equals("0"))
            seclogin = Util.prop.getProperty("sec_login");

        datos = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_login.txt", seclogin);
        if (datos == null)
            System.out.println("No hay datos en datapool");
        else
        {
            Util.setDataCliente(datos);
            System.out.println("setUp==>Datos Leidos de datapool");
        }
    }

    void click_menu_aplicacion()
    {
        List<WebElement> menu_aplicacion = Util.driver.findElements(By.name("Aplicación"));
        String actual = menu_aplicacion.get(1).getText();
        Util.assert_contiene("LOGIN", "Da click en menú", actual,"Aplicación", true, "N");
        menu_aplicacion.get(1).click();
    }

    void click_menu_logon()
    {
        List<WebElement> menu_logon = Util.driver.findElements(By.name("Log on"));
        String actual = menu_logon.get(0).getText();
        Util.assert_contiene("LOGIN", "Da click en submenú", actual,"Log on", true, "N");
        menu_logon.get(0).click();
    }
    void click_menu_imprimir()
    {
        List<WebElement> menu_imprimir = Util.driver.findElements(By.name("Imprimir"));
        String actual = menu_imprimir.get(0).getText();
        Util.assert_contiene("LOGIN", "Da click en submenú", actual,"Imprimir", true, "N");
        menu_imprimir.get(0).click();
    }

    void ingreso_datos1()
    {
        List<WebElement> edit = Util.driver.findElements(By.className("Edit"));
        edit.get(1).sendKeys(Util.getDataCliente()[1]);//"SRVDESA2"
        edit.get(0).sendKeys(Util.getDataCliente()[2]);//USUARIO
    }

    void ingreso_datos2()
    {
        WebElement password = Util.driver.findElement(By.className("ThunderRT6TextBox"));
        password.sendKeys(Util.getDataCliente()[3]);
        String actual = "Servidor: " + Util.getDataCliente()[1] + "</b><br>"
                + "Usuario: " + Util.getDataCliente()[2]  + "</b><br>" + "Contraseña: " + Util.getDataCliente()[3];
        Reporte.agregarPaso("LOGIN", "Ingresa datos de login", actual, "", true, "N");
    }

    void click_boton_ok() {
        WebElement boton = Util.driver.findElement(By.name("OK"));
        String actual = boton.getText();
        Util.assert_contiene("LOGIN", "Da click en botón OK", actual,"OK", true, "N");
        boton.click();
    }
    void click_boton_aceptar()
    {
        WebElement boton = Util.driver.findElement(By.className("Button"));
        String actual = boton.getText();
        Util.assert_contiene("LOGIN", "Da click en botón Aceptar", actual,"Aceptar", true, "N");
        boton.click();
    }

    void escogeRol() {
        int i = 0;

        while (i != Integer.parseInt(Util.getDataCliente()[5])) {
            Util.driver.findElementByName("Línea abajo").click();
            i++;
        }
        Actions action = new Actions(Util.driver);
        action.moveToElement(Util.driver.findElementByName("Datos Generales"), 0, 0).moveByOffset(40, 160).click().perform();

    }
        void click_boton_ok1() throws InterruptedException {
            Thread.sleep(5000);
            String[] allWindowHandles = Util.driver.getWindowHandles().toArray(new String[0]);
            Util.driver.switchTo().window(allWindowHandles[0]);

            List<WebElement> botones = Util.driver.findElements(By.name("OK"));
            WebElement actual = botones.get(1);
            //Util.assert_contiene("LOGIN", "Verificación de etiqueta Usuario", actual,"Usuario", true, "N");
            actual.click();
        }
   void aceptar_aperturaCaja()
    {
        try {
            Util.waitForElementToBeClickable(By.className("Static"));
            List<WebElement> texto = Util.driver.findElements(By.className("Static"));
            String actual = texto.get(0).getText();
            Util.assert_contiene("LOGIN", "Verificación apertura caja", actual, "[sp_verifica_caja_rc] CAJERO NO REGISTRADO", true, "N");
            this.click_boton_aceptar();
            this.aperturarCaja = true;
        }
        catch(Exception e)
        {
            System.out.println("Caja aperturada");
        }
    }

    public void ingresar() throws InterruptedException {

        String[] allWindowHandles = Util.driver.getWindowHandles().toArray(new String[0]);
        Util.driver.switchTo().window(allWindowHandles[0]);

        this.cargaDatosLogin("5");
        this.click_menu_aplicacion();
        this.click_menu_imprimir();
        this.click_menu_aplicacion();
        this.click_menu_logon();
        this.ingreso_datos1();
        this.ingreso_datos2();
        this.click_boton_ok();
        this.click_boton_aceptar();
        this.escogeRol();
        this.click_boton_ok();
        this.click_boton_aceptar();
        this.click_boton_ok1();
        this.aceptar_aperturaCaja();

    }


}
