package TestPages;

import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import java.util.List;

public class aperturaCaja {
    Actions action = new Actions(Util.driver);
    void ingreso_nemonico()
    {
        WebElement ingresonemocio= Util.driver.findElementByAccessibilityId("4");
        ingresonemocio.sendKeys("05" + Keys.ENTER);
    }
    void click_boton_aceptar()
    {
        WebElement boton = Util.driver.findElement(By.className("Button"));
        String actual = boton.getText();
        Util.assert_contiene("LOGIN", "Da click en botón Aceptar", actual,"Aceptar", true, "N");
        boton.click();
    }

    void click_añadir() {
        List<WebElement> añadir = Util.driver.findElements(By.name("Añadir"));
        String actual = añadir.get(0).getText();
        System.out.println(actual);
        //Util.assert_contiene("LOGIN", "Verificación de etiqueta Usuario", actual,"Usuario", true, "N");
        añadir.get(0).click();
    }
     void click_boton_Si() throws InterruptedException {
         //Util.driver.findElementByName("AtxVB");
        Util.driver.findElementByName("Sí").click();
        //Util.driver.findElementByAccessibilityId("6").click();
       }

    void click_F1() {
        Actions action = new Actions(Util.driver);
        action.sendKeys(Keys.F1).click().perform();
    }
    void Usuari_admin() throws InterruptedException {
        Thread.sleep(2000);
        action.sendKeys("cloor").perform();
        action.sendKeys(Keys.TAB).perform();
        action.sendKeys("12345678").perform();
        action.sendKeys(Keys.ENTER).perform();
    }
       void Varios_Aceptar(){

            action.sendKeys(Keys.ENTER).perform();
            action.sendKeys(Keys.ENTER).perform();
            action.sendKeys(Keys.ENTER).perform();
            action.sendKeys(Keys.ENTER).perform();
            action.sendKeys(Keys.ENTER).perform();
            action.sendKeys(Keys.ENTER).perform();
            action.sendKeys(Keys.ENTER).perform();
            action.sendKeys(Keys.ENTER).perform();
    }


    public void aperturar() throws InterruptedException {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String[] allWindowHandles = Util.driver.getWindowHandles().toArray(new String[0]);
        Util.driver.switchTo().window(allWindowHandles[0]);

        this.ingreso_nemonico();
        this.click_boton_aceptar();
        this.click_añadir();
        this.click_boton_Si();
       this.click_F1();
        this.click_boton_Si();
        this.Usuari_admin();
        //this.click_boton_aceptar();
        //this.Varios_Aceptar();


    }
}
