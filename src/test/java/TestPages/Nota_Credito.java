package TestPages;

import Globales.Reporte;
import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class Nota_Credito {
    Actions action = new Actions(Util.driver);
    String[] datos = null;
    void cargaDatosNotaCredito(String seclogin) {
        if (seclogin.equals("0"))
            seclogin = Util.prop.getProperty("sec_login");

        datos = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_notaCredito.txt", seclogin);
        if (datos == null)
            System.out.println("No hay datos en datapool");
        else
        {
            Util.setDataCliente(datos);
            System.out.println("setUp==>Datos Leidos de datapool");
        }
    }

    void ingreso_nemonico()
    {
        WebElement ingresonemocio= Util.driver.findElementByAccessibilityId("4");
        ingresonemocio.sendKeys("116" + Keys.ENTER);
        Reporte.agregarPaso("Generación Nota Credito", "Ingresar Nemonico 116", "", "", true, "N");
    }

    void ingresar_tipo_cuenta(){
        if(Util.getDataCliente()[1].equals("AHO"))
        {
            WebElement clic_tipo_cuenta=Util.driver.findElementByAccessibilityId("2");
            clic_tipo_cuenta.click();
            clic_tipo_cuenta.sendKeys("A");
            clic_tipo_cuenta.click();
            Reporte.agregarPaso("Generación Nota Credito", "Selecciona tipo de cuenta AHO", "", "", true, "N");


        }else if(Util.getDataCliente()[1].equals("CTE")){
            WebElement clic_tipo_cuenta=Util.driver.findElementByAccessibilityId("2");
            clic_tipo_cuenta.click();
            clic_tipo_cuenta.sendKeys("C");
            clic_tipo_cuenta.click();
            Reporte.agregarPaso("Generación Nota Credito", "Selecciona el tipo de cuenta CTE", "", "", true, "N");

        }else{
            System.out.println("INGRESE EN EL TXT LA OPCIÓN AHO/CTE");
        }
    }

    void ingresa_cuenta(){
        List<WebElement> ingresar_cuenta=Util.driver.findElements(By.className("Edit"));
        ingresar_cuenta.get(1).sendKeys(Util.getDataCliente()[2]);
        Reporte.agregarPaso("Generación Nota Credito", "Ingresar el número de cuenta", "", "", true, "N");


    }
    void clic_varios_Aceptar(){

        action.sendKeys(Keys.ENTER).perform();
        action.sendKeys(Keys.ENTER).perform();
        action.sendKeys(Keys.ENTER).perform();
        action.sendKeys(Keys.ENTER).perform();
    }
    void ingresa_cartilla() throws InterruptedException {
        Thread.sleep(1000);
        List<WebElement> ingresar_cartilla=Util.driver.findElements(By.className("Edit"));
        ingresar_cartilla.get(5).sendKeys(Util.getDataCliente()[3]);
        ingresar_cartilla.get(6).sendKeys(Util.getDataCliente()[4]);
        Reporte.agregarPaso("Generación Nota Credito", "Ingresar el número de cartilla", "", "", true, "N");


    }

    void ingresa_tipo_causa() throws InterruptedException {
        Thread.sleep(1000);
        List<WebElement> ingresar_causa=Util.driver.findElements(By.className("Edit"));
        ingresar_causa.get(2).sendKeys(Util.getDataCliente()[6]);
        Reporte.agregarPaso("Generación Nota Credito", "Ingresar el tipo de causa", "", "", true, "N");

    }
    void ingresa_departamento() throws InterruptedException {
        Thread.sleep(1000);
        List<WebElement> ingresar_departamento=Util.driver.findElements(By.className("Edit"));
        ingresar_departamento.get(4).sendKeys(Util.getDataCliente()[7]);
        Reporte.agregarPaso("Generación Nota Credito", "Ingresar el número de departamento", "", "", true, "N");

    }
    void ingresar_referencia(){
        List<WebElement> ingresar_referencia=Util.driver.findElementsByAccessibilityId("4");
        ingresar_referencia.get(1).sendKeys(Util.getDataCliente()[8]);
        Reporte.agregarPaso("Generación Nota Credito", "Se ingresa la  referencia por la nota de debito ", "", "", true, "N");

    }
    void ingresa_monto() throws InterruptedException {
        Thread.sleep(1000);
        List<WebElement> ingresar_monto=Util.driver.findElements(By.className("AfxWnd40"));
        ingresar_monto.get(10).sendKeys(Util.getDataCliente()[9]);
        Reporte.agregarPaso("Generación Nota Credito", "Ingresar el monto", "", "", true, "N");

    }
    void click_F1() {
        Actions action = new Actions(Util.driver);
        action.sendKeys(Keys.F1).click().perform();
    }



    public void generar() throws InterruptedException {
        this.cargaDatosNotaCredito("2");
        this.ingreso_nemonico();
        this.ingresar_tipo_cuenta();
        this.ingresa_cuenta();
        this.clic_varios_Aceptar();
        this.ingresa_tipo_causa();
        this.ingresa_departamento();
        this.ingresar_referencia();
        this.ingresa_monto();
        this.click_F1();


    }
}
