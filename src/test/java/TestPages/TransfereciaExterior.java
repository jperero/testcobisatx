package TestPages;

import Globales.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class TransfereciaExterior {
    Actions action = new Actions(Util.driver);
    String[] datos = null;
    void cargaDatosLogin(String seclogin) {
        if (seclogin.equals("0"))
            seclogin = Util.prop.getProperty("sec_login");

        datos = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_transferencias_exterior.txt", seclogin);
        if (datos == null)
            System.out.println("No hay datos en datapool");
        else
        {
            Util.setDataCliente(datos);
            System.out.println("setUp==>Datos Leidos de datapool");
        }
    }

    void ingreso_nemonico()
    {
        WebElement ingresonemocio = Util.driver.findElement(By.className("ThunderRT6TextBox"));
        ingresonemocio.sendKeys("123" + Keys.ENTER);
    }
    void ingresar_tipo_trasnferencia() throws InterruptedException {
        Util.driver.findElementByAccessibilityId("DropDown").sendKeys("N");
        Util.driver.findElementByAccessibilityId("3").sendKeys(Util.getDataCliente()[1]);
        action.sendKeys(Keys.TAB).perform();
        action.sendKeys(Keys.ENTER).perform();
        Thread.sleep(2000);
    }
    void click_maximizar()
    {

        Util.driver.findElementByName("Maximizar").click();
    }
    void tab_cuenta()
    {
        for (int i = 0; i < 3; i++) {//UBICACION CUENTA
            action.sendKeys(Keys.TAB).perform();
        }
        action.sendKeys(Util.getDataCliente()[2]).perform();
    }

    void click_firmas() throws InterruptedException
    {
        for (int i = 0; i < 4; i++) {//ACEPTAR FIRMAS
            action.sendKeys(Keys.ENTER).perform();
            Thread.sleep(1000);
        }
    }

    void tab_bco_destinatario() throws InterruptedException {
        for (int i = 0; i < 2; i++) {//UBICACION BCO DESTINATARIO
            action.sendKeys(Keys.TAB).perform();
        }
        action.sendKeys(Util.getDataCliente()[3]).perform();
        action.sendKeys(Keys.TAB).perform();
        Thread.sleep(2000);
    }
    void cuenta_destino() throws InterruptedException {
        action.sendKeys(Util.getDataCliente()[4]).perform();//cuenta destino
        action.sendKeys(Keys.TAB);
        Thread.sleep(2000);

    }
    void nombre_cliente() throws InterruptedException {
        action.sendKeys(Util.getDataCliente()[5]).perform();//nombre de cuenta cliente
        action.sendKeys(Keys.TAB);
        Thread.sleep(2000);

    }
    void dirreccion() throws InterruptedException {
        action.sendKeys(Util.getDataCliente()[6]).perform();//direccion
        action.sendKeys(Keys.TAB);
        Thread.sleep(2000);

    }
    void telefono()
    {
        action.sendKeys(Util.getDataCliente()[7]).perform();
    }
    void monto() throws InterruptedException {
        for (int i = 0; i < 2; i++) {//UBICACION MONTO
            action.sendKeys(Keys.TAB).perform();
        }
        Thread.sleep(2000);
        action.sendKeys(Util.getDataCliente()[8]).perform();//monto
        action.sendKeys(Keys.TAB).perform();
        Thread.sleep(2000);
    }
    void motivo()
    {
        action.sendKeys(Util.getDataCliente()[9]).perform();//motivo
        action.sendKeys(Keys.F1).perform();
    }
    void aceptar_firmas() throws InterruptedException {
        for (int i = 0; i < 6; i++) {//ACEPTAR FIRMAS
            action.sendKeys(Keys.ENTER).perform();
            Thread.sleep(1000);
        }
        action.sendKeys(Keys.ARROW_LEFT).perform();
        for (int i = 0; i < 6; i++) {//ACEPTAR FIRMAS
            action.sendKeys(Keys.ENTER).perform();
            Thread.sleep(1000);
        }
        action.sendKeys(Keys.ARROW_LEFT).perform();
        for (int i = 0; i < 6; i++) {//ACEPTAR FIRMAS
            action.sendKeys(Keys.ENTER).perform();
            Thread.sleep(1000);
        }
        action.sendKeys(Keys.ARROW_LEFT).perform();
        for (int i = 0; i < 4; i++) {//ACEPTAR FIRMAS
            action.sendKeys(Keys.ENTER).perform();
            Thread.sleep(1000);
        }

    }
       void cerrar_ventana(){
        action.sendKeys(Keys.ESCAPE).perform();
        action.sendKeys(Keys.ESCAPE).perform();
    }

    public void ingresar() throws InterruptedException {
        Thread.sleep(3000);
        String[] allWindowHandles = Util.driver.getWindowHandles().toArray(new String[0]);
        Util.driver.switchTo().window(allWindowHandles[0]);

        this.cargaDatosLogin("1");
        this.ingreso_nemonico();
        this.ingresar_tipo_trasnferencia();
        this.click_maximizar();
        this.tab_cuenta();
        this.click_firmas();
        this.tab_bco_destinatario();
        this.cuenta_destino();
        this.nombre_cliente();
        this.dirreccion();
        this.telefono();
        this.monto();
        this.motivo();
        this.aceptar_firmas();
        this.cerrar_ventana();
    }

}