package RunScripts;

import Globales.Reporte;
import Globales.Util;
import TestPages.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;

public class RunNotaDebito {

    @Before
    public void iniciar_Chrome() throws MalformedURLException {
        Util.Inicio("Generar Nota Debito");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Modulo: CobisAtx " );
    }

    @Test
    public void  Generar_Nota_Debito() throws InterruptedException {
        Reporte.setNombreReporte("Generar Nota Debito Exitoso!");
        Login login = new Login();
        login.ingresar();

        if (login.aperturarCaja)
        {
            aperturaCaja caja = new aperturaCaja();
            caja.aperturar();
        }

        Nota_Debito nota=new Nota_Debito();
        nota.generar();
    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }
}



