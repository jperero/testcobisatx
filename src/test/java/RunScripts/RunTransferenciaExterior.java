package RunScripts;

import Globales.Reporte;
import Globales.Util;
import TestPages.Login;
import TestPages.TransfereciaExterior;
import TestPages.aperturaCaja;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;

public class RunTransferenciaExterior {

    @Before
    public void Inicio_Tservi () throws MalformedURLException {
        Util.Inicio("Login");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "M�dulo: CobisAtx " );
    }

    @Test
    public void Transferencia_Exterior() throws InterruptedException {
        Reporte.setNombreReporte("Login Exitoso");
        Login login = new Login();
        login.ingresar();

        if (login.aperturarCaja)
        {
            aperturaCaja caja = new aperturaCaja();
            caja.aperturar();
        }
        TransfereciaExterior transferencia=new TransfereciaExterior();
        transferencia.ingresar();
    }
    @After
    public void salir()
    {
        Reporte.finReporte();
    }
}
