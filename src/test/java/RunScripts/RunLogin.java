package RunScripts;
import Globales.Reporte;
import Globales.Util;
import TestPages.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;

public class RunLogin {

    @Before
    public void iniciar_Chrome() throws MalformedURLException {
        Util.Inicio("Login");
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Modulo: CobisAtx " );
    }

    @Test
    public void  LoginTest() throws InterruptedException {
        Reporte.setNombreReporte("Login Exitoso");
        Login login = new Login();
        login.ingresar();

       if (login.aperturarCaja)
        {
            aperturaCaja caja = new aperturaCaja();
            caja.aperturar();
        }
    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }
}

