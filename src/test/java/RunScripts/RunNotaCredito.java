package RunScripts;

import Globales.Reporte;
import Globales.Util;
import TestPages.Login;
import TestPages.Nota_Credito;
import TestPages.aperturaCaja;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;

public class RunNotaCredito {

    @Before
    public void iniciar_Chrome() throws MalformedURLException {
        Util.Inicio("Generar Nota Credito");
        Reporte.setEntorno("Ambiente: Desarrollo1" + "</b><br>" + "Modulo: CobisAtx " );
    }

    @Test
    public void  Generar_Nota_Credito() throws InterruptedException {
        Reporte.setNombreReporte("Generar Nota Credito Exitoso!");
        Login login = new Login();
        login.ingresar();

        if (login.aperturarCaja)
        {
            aperturaCaja caja = new aperturaCaja();
            caja.aperturar();
        }
        Nota_Credito nota=new Nota_Credito();
        nota.generar();
    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }
}



